# deluge-ui

A mobile UI that uses an intermediate Rest API to work with Deluge.

The intermediate API is able to bypass Deluge CORS restrictions (headers) and handles the authentication process.

This project is intended to be used with password protection at the web server level.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
