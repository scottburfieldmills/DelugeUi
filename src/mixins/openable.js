export default {
  props: ['show'],
  methods: {
    close () {
      this.$emit('close')
    }
  }
}
