import axios from 'axios'

class TorrentService {
  constructor () {
    if (process.env.NODE_ENV === 'development') {
      this.url = 'http://localhost:61656/api/torrents'
    } else {
      this.url = 'http://delugeapi.scottbm.me/api/torrents'
    }
  }

  get (successCallback, errorCallback) {
    let options = { crossDomain: true, timeout: 10 * 1000 }

    axios.get(this.url, options)
      .then(successCallback)
      .catch(errorCallback)
  }

  add (torrent, successCallback, errorCallback) {
    let payload = { torrent: torrent }
    let options = { timeout: 10 * 1000 }

    axios.post(this.url, payload, options)
      .then(successCallback)
      .catch(errorCallback)
  }

  delete (torrent, successCallback, errorCallback) {
    let options = { timeout: 10 * 1000 }

    axios.delete(this.url + '/' + torrent.Hash, options)
      .then(successCallback)
      .catch(errorCallback)
  }
}

export default TorrentService
