import Vue from 'vue'
import App from './App'
import router from './router'

import Raven from '../node_modules/raven-js/dist/raven.js'
Raven.config('https://6e1716cc446a41dd98776e23dfe6431e@sentry.io/179862').install()

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
